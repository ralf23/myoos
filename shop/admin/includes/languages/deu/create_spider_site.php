<?php
/* ----------------------------------------------------------------------
   $Id: create_spider_site.php,v 1.3 2007/06/13 16:15:14 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/
   
   
   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2003 osCommerce
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */

define('HEADING_TITLE', 'Artikelbersicht f&uuml;r Suchmaschinen');
define('TABLE_HEADING_ALL', 'Anzahl Artikel');
define('TABLE_HEADING_EX', 'Exportiert');
define('TABLE_HEADING_FILTER', 'Ignoriert wegen Filter');

define('TEXT_INFO_LOCATION', 'Die Spiderseite wurde erfolgreich in ');
define('TEXT_LOCATION', OOS_ABSOLUTE_PATH . 'spider.html');
define('TEXT_SAVED', ' erstellt.');


