<?php
/* ----------------------------------------------------------------------
   $Id: stats_referer.php,v 1.3 2007/06/13 16:15:14 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/

   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */

define('HEADING_TITLE', 'Wer linkt zum Shop');

define('TABLE_HEADING_NUMBER', 'Nr.');
define('TABLE_HEADING_FREQUENCY', 'Besucher');
define('TABLE_HEADING_URL', 'URL');
define('TABLE_HEADING_PER_CENT', '%');

define('TEXT_HTTP_REFERERS_RESET', 'HTTP referers wurden zur&uuml;ck gestellt!');


