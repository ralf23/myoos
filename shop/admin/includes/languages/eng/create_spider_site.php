<?php
/* ----------------------------------------------------------------------
   $Id: create_spider_site.php,v 1.3 2007/06/13 16:38:21 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/
   
   
   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2003 osCommerce
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */

define('HEADING_TITLE', 'Article reports for search machines');
define('TABLE_HEADING_ALL', 'Number of articles');
define('TABLE_HEADING_EX', 'Exported');
define('TABLE_HEADING_FILTER', 'Ignored because of filters');

define('TEXT_INFO_LOCATION', 'The Spidersite became successful in ');
define('TEXT_LOCATION', OOS_ABSOLUTE_PATH . 'spider.html');
define('TEXT_SAVED', ' provided.');


