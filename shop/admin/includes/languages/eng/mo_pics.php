<?php
/* ----------------------------------------------------------------------
   $Id: mo_pics.php,v 1.3 2007/06/13 16:38:21 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/
   
   
   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Based on:
   
   mo_pics.php
   BOF: WebMakers.com Added: mo images with modifications - combined work of: John Poynton, Richard Aspden, Brandon Sweet
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */

// BOF: WebMakers.com Added: mo images with modifications - combined work of: John Poynton, Richard Aspden, Brandon Sweet
define('TEXT_PRODUCTS_BIMAGE', 'Big Products Image:');
define('TEXT_PRODUCTS_SUBIMAGE1', 'SubImage 1:');
define('TEXT_PRODUCTS_BSUBIMAGE1', 'Big SubImage 1:');
define('TEXT_PRODUCTS_SUBIMAGE2', 'SubImage 2:');
define('TEXT_PRODUCTS_BSUBIMAGE2', 'Big SubImage 2:');
define('TEXT_PRODUCTS_SUBIMAGE3', 'SubImage 3:');
define('TEXT_PRODUCTS_BSUBIMAGE3', 'Big SubImage 3:');
define('TEXT_PRODUCTS_SUBIMAGE4', 'SubImage 4:');
define('TEXT_PRODUCTS_BSUBIMAGE4', 'Big SubImage 4:');
define('TEXT_PRODUCTS_SUBIMAGE5', 'SubImage 5:');
define('TEXT_PRODUCTS_BSUBIMAGE5', 'Big SubImage 5:');
define('TEXT_PRODUCTS_SUBIMAGE6', 'SubImage 6:');
define('TEXT_PRODUCTS_BSUBIMAGE6', 'Big SubImage 6:');
// BOF: WebMakers.com Added: mo images with modifications - combined work of: John Poynton, Richard Aspden, Brandon Sweet

