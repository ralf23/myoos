<?php
/* ----------------------------------------------------------------------
   $Id: stats_referer.php,v 1.3 2007/06/13 16:38:21 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/

   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */

define('HEADING_TITLE', 'Who\'s linking to this site');

define('TABLE_HEADING_NUMBER', 'Nr.');
define('TABLE_HEADING_FREQUENCY', 'Frequency');
define('TABLE_HEADING_URL', 'URL');
define('TABLE_HEADING_PER_CENT', 'per cent');

define('TEXT_HTTP_REFERERS_RESET', 'The HTTP referers counter was reset!');


