<?php
/* ----------------------------------------------------------------------
   $Id: deu.php,v 1.1 2007/06/07 17:29:24 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/

   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */

define('PLUGIN_EVENT_CURRENCIES_NAME', 'W&auml;hrungen');
define('PLUGIN_EVENT_CURRENCIES_DESC', 'Standard W&auml;hrung oder gew&auml;hlte');

