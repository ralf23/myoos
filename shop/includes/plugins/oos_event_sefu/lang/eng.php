<?php
/* ----------------------------------------------------------------------
   $Id: eng.php,v 1.1 2007/06/13 15:41:56 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/

   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */

define('PLUGIN_EVENT_SEFU_NAME', 'Search Engine Friendly URLs');
define('PLUGIN_EVENT_SEFU_DESC', 'Search engine friendly urls.');

