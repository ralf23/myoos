<?php
/* ----------------------------------------------------------------------
   $Id: newsletters_unsubscribe_success.php,v 1.3 2007/06/12 16:51:19 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/
   
   
   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */


$aLang['navbar_title_1'] = 'Newsletter';
$aLang['navbar_title_2'] = 'Erfolg';
$aLang['heading_title'] = 'Sie wurden aus der Mailingliste ausgetragen!';
$aLang['text_main'] = '';

