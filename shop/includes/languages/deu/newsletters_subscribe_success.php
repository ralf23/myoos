<?php
/* ----------------------------------------------------------------------
   $Id: newsletters_subscribe_success.php,v 1.3 2007/06/12 16:36:39 r23 Exp $

   MyOOS [Shopsystem]
   http://www.oos-shop.de/
   
   
   Copyright (c) 2003 - 2014 by the MyOOS Development Team.
   ----------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------- */

$aLang['navbar_title_1'] = 'Newsletter';
$aLang['navbar_title_2'] = 'Gl&uuml;ckwunsch';
$aLang['heading_title'] = 'Gl&uuml;ckwunsch,';
$aLang['text_main'] = 'Sie wurden soeben erfolgreich f&uuml;r den ' . STORE_NAME . ' Newsletter registriert. <br><br>Sie erhalten unseren Newsletter indem wir Sie &uuml;ber unsere Produktneuheiten informieren.';


