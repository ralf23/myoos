
$aLang['navbar_title_1'] = 'Anmelden';
$aLang['navbar_title_2'] = 'Passwort vergessen';
$aLang['heading_title'] = 'Wie war noch mal mein Passwort?';
$aLang['text_new_customer'] = 'Sind Sie Neukunde?';
$aLang['text_new_customer_introduction'] = 'Jetzt registrieren.';
$aLang['text_no_email_address_found'] = 'Die eingegebene eMail-Adresse ist nicht registriert. Bitte versuchen Sie es noch einmal.';
$aLang['email_password_reminder_subject'] = STORE_NAME . ' - Ihr neues Passwort.';
$aLang['email_password_reminder_body'] = 'Über die Adresse ' . oos_server_get_remote() . ' haben wir eine Anfrage zur Passworterneuerung erhalten.' . "\n\n" . 'Ihr neues Passwort für \'' . STORE_NAME . '\' lautet ab sofort:' . "\n\n" . '   %s' . "\n\n";
$aLang['text_password_sent'] = 'Ein neues Passwort wurde per eMail verschickt.';

