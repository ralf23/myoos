{include file="myoos/system/_header.tpl"}
	<!-- Wrapper -->
	<div class="wrapper">
    <section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="inner-heading">
                    <h2>{$heading_title}</h2>
                </div>
            </div>
            <div class="col-md-8">
                {$breadcrumb}
            </div>
        </div>
    </div>
    </section>
    <section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {$informations.information_description}
            </div>
        </div>
    </div>
    </section>
	</div> <!-- / .wrapper -->
{include file="myoos/system/_footer.tpl"}